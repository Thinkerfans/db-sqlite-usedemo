package com.android.use.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.use.database.PersonDatabase;
import com.android.use.mode.Person;

public class MainActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	
		pd = PersonDatabase.newInstance(this);
		
		p = new Person("persion id "+id,"name "+id,id);
	
		
	}
	PersonDatabase pd;
	Person p = null;
	int id = 0;

	public void onClick(View v) {
		boolean re = false;
		switch (v.getId()) {
		case R.id.insert:
			p = new Person("persion_id","name "+id,id);
			re = pd.insert(p);
			Toast.makeText(this, p.toString()+re, Toast.LENGTH_LONG).show();		
			break;
	  case R.id.delete:
		   re = pd.delete(p); 
			break;
	  case R.id.query:
		   p = pd.read(p.getId());
		   if(p != null)
			   Toast.makeText(this, p.toString(), Toast.LENGTH_LONG).show();
		   else
			   Toast.makeText(this, "p is null", Toast.LENGTH_LONG).show();
		break;

	  case R.id.update:
		  p.setAge(p.getAge()+10);
		  pd.update(p);
		   Toast.makeText(this, p.toString(), Toast.LENGTH_LONG).show();
		break;

		default:
			break;
		}
	}
}
