package com.android.use.database;

import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBOpenHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "test.db";
	private static final int DATABASE_VERSION = 1;
	private final ReentrantLock mReentrantLock = new ReentrantLock(true);
	
	private DBOpenHelper(Context ctx){
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);	
	}
	
	public static DBOpenHelper sInstance;
	public static synchronized DBOpenHelper getInstance(Context ctx){
		if( null == sInstance ){
			sInstance = new DBOpenHelper(ctx);
		}
		return sInstance;
	}
	
	public ReentrantLock getLock(){
		return mReentrantLock;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		createTestTable(db);
	}

	private void createTestTable(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + PersonDatabase.TABLE_NAME
				+ "(" 
				+ PersonDatabase.COL_ID + " text primary key,"
				+ PersonDatabase.COL_NAME+" text,"
				+ PersonDatabase.COL_AGE +" integer )");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub		
	}
}
