package com.android.use.database;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.android.use.mode.Person;

public class PersonDatabase {
	public static final String TABLE_NAME = "person";

	public static final String COL_ID = "id";
	public static final String COL_NAME = "name";
	public static final String COL_AGE = "age";

	private DBOpenHelper mDbOpenHelper;
	private ReentrantLock mLock;
	private static PersonDatabase mDatabase;

	private PersonDatabase(Context ctx) {
		mDbOpenHelper = DBOpenHelper.getInstance(ctx);
		mLock = mDbOpenHelper.getLock();
	}

	public synchronized static PersonDatabase newInstance(Context ctx) {
		if (null == mDatabase) {
			mDatabase = new PersonDatabase(ctx);
		}
		return mDatabase;
	}

	public boolean isExist(Person p) {
		if (p == null) {
			throw new IllegalStateException("Person parament can not be null");
		}

		if (TextUtils.isEmpty(p.getId())) {
			throw new IllegalStateException(
					"id must not be null and size must > zero");
		}

		SQLiteDatabase db = null;
		Cursor c = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getReadableDatabase();

			String[] columns = new String[] { COL_ID };
			String selection = COL_ID + "=?";
			String[] selectionArgs = new String[] { p.getId() };

			c = db.query(TABLE_NAME, columns, selection, selectionArgs, null,
					null, null);

			if (c.moveToFirst()) {
				return true;
			}
			return false;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				c.close();
			}
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return false;
	}

	public boolean insert(Person p) {
		if (p == null) {
			throw new IllegalStateException("Person parament can not be null");
		}
		//TODO 
		if(isExist(p)){
			return update(p);
		}
		
		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			/*
			String sql = "INSERT INTO " + TABLE_NAME + "(" + COL_ID + ","
					 + COL_NAME + "," + COL_AGE + ")VALUES(?,?,?)";	
					Object[] obj = new Object[] {   p.getId(),p.getName(), p.getAge()};
			db.execSQL(sql, obj);
			*/		
			ContentValues values = new ContentValues();
			values.put(COL_ID, p.getId());
			values.put(COL_NAME, p.getName());
			values.put(COL_AGE, p.getAge());
			long ret = db.insert(TABLE_NAME, null, values);
			return ret != -1;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}

		return false;
	}

	public boolean delete(Person p) {
		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			
/* String handler method 
			String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + COL_ID
					+ "=?";
			db.execSQL(sql, new Object[] { p.getId() });
*/	
			
	        String where = COL_ID + "=?";
	        String[] whereValue={p.getId()};
	        db.delete(TABLE_NAME, where, whereValue);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return false;
	}

	public Person read(String id) {
		if (TextUtils.isEmpty(id)) {
			throw new IllegalStateException(
					"id must not be null and size must > zero");
		}

		Person p = null;
		Cursor c = null;
		SQLiteDatabase db = null;

		try {
			mLock.lock();
			db = mDbOpenHelper.getReadableDatabase();

			String[] columns = new String[] { COL_ID,COL_NAME,COL_AGE };
			String selection = COL_ID + "=?";
			String[] selectionArgs = new String[] { id };
			c = db.query(TABLE_NAME, columns, selection, selectionArgs, null,
					null, null);
/*
			 String sql = "select * from " + TABLE_NAME + " WHERE " + COL_ID+"=?";
			 c = db.rawQuery(sql, new String[]{id});
*/			 
			while(c.moveToNext()) {
				p = new Person();
				p.setId(id);
				p.setName(c.getString(c.getColumnIndex(COL_NAME)));
				p.setAge(c.getInt(c.getColumnIndex(COL_AGE)));
				return p;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				c.close();
			}
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return p;
	}

	public ArrayList<Person> read() {
		ArrayList<Person> pList = new ArrayList<Person>();
		Cursor c = null;
		SQLiteDatabase db = null;

		try {
			mLock.lock();
			db = mDbOpenHelper.getReadableDatabase();
/*		
			String sql = "select * from " + TABLE_NAME;
			c = db.rawQuery(sql, null);
*/
			String[] columns = new String[] { COL_ID,COL_NAME,COL_AGE };
			String selection = COL_ID + "=?";
			c = db.query(TABLE_NAME, columns, selection, null, null, null, null);

			if (c.moveToNext()) {
				Person p = new Person();
				p.setId(c.getString(c.getColumnIndex(COL_ID)));
				p.setName(c.getString(c.getColumnIndex(COL_NAME)));
				p.setAge(c.getInt(c.getColumnIndex(COL_AGE)));
				pList.add(p);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				c.close();
			}
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return pList;
	}

	public boolean update(Person p) {
		if (p == null) {
			throw new IllegalStateException("Person parament can not be null");
		}

		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			
/*
			String sql = "UPDATE " + TABLE_NAME + " SET " +COL_NAME + "=?" + "," + COL_AGE + "=?"
			 + " WHERE " + COL_ID + "=?";		
			Object[] obj = new Object[] { p.getName(), p.getAge(),  p.getId()};
			db.execSQL(sql, obj);
*/
			ContentValues values = new ContentValues();
			values.put(COL_ID, p.getId());
			values.put(COL_NAME, p.getName());
			values.put(COL_AGE, p.getAge());
			String whereClause = COL_ID + "=?";
			String[] whereArgs = new String[] { p.getId() };
			long ret = db.update(TABLE_NAME, values, whereClause, whereArgs);
			return ret != 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return false;
	}
}
